# P

## Content

```
./Pascal Bruckner:
Pascal Bruckner - Paradoxul iubirii.pdf

./Patrick Gardiner:
Patrick Gardiner - Kierkegaard (Maestrii spiritului).pdf

./Patrick Suppes:
Patrick Suppes - Metafizica probabilista.pdf

./Paul Davies:
Paul Davies - Ultimele trei minute.pdf

./Paul Deussen:
Paul Deussen - Filosofia Upanisadelor.pdf

./Paul Henry Chombart de Lauwe:
Paul Henry Chombart de Lauwe - Cultura si puterea.pdf

./Paul Kennedy:
Paul Kennedy - Ascensiunea si decaderea marilor puteri.pdf

./Paul Ludwig Landsberg:
Paul Ludwig Landsberg - Eseu despre experienta mortii.pdf

./Paul Ricoeur:
Paul Ricoeur - Eseuri de hermeneutica.pdf
Paul Ricoeur - Istorie si adevar.pdf
Paul Ricoeur - La scoala fenomenologiei.pdf

./Pedro Gonzales Calero:
Pedro Gonzales Calero - Filosofia pentru bufoni.pdf

./Peter Godfrey Smith:
Peter Godfrey Smith - Filosofia stiintei.pdf

./Peter Kampits:
Peter Kampits - Intre aparenta si realitate.pdf

./Peter Kapitza:
Peter Kapitza - Experiment, teorie, practica.pdf

./Peter King:
Peter King - 100 de filosofi.pdf

./Peter Singer:
Peter Singer - Hegel (Maestrii spiritului).pdf
Peter Singer - Tratat de etica.pdf

./Peter Sloterdijk:
Peter Sloterdijk - Critica ratiunii cinice, Vol 1.pdf
Peter Sloterdijk - Critica ratiunii cinice, Vol 2.pdf

./Peter William Atkins:
Peter William Atkins - Regatul periodic.pdf

./Petre Andrei:
Petre Andrei - Filosofia valorii.pdf

./Petre Bieltz:
Petre Bieltz - Principiul dualitatii in logica formala.pdf

./Petre Botezatu:
Petre Botezatu - Cauzalitatea fizica si panquantismul.pdf
Petre Botezatu - Introducere in logica.pdf

./Petre Botezatu & T. Dima:
Petre Botezatu & T. Dima - Directii in logica contemporana.pdf

./Petre Paul Negulescu:
Petre Paul Negulescu - Geneza formelor culturii.pdf
Petre Paul Negulescu - Istoria filosofiei contemporane, vol. 1.pdf
Petre Paul Negulescu - Istoria filosofiei contemporane, vol. 2.pdf

./Petru Ioan:
Petru Ioan - Logica si metalogica.pdf

./Philolaos:
Philolaos - Despre Pythagora si Pythagorei.pdf

./Pierre Abelard:
Pierre Abelard - Comentarii la Porfir.pdf
Pierre Abelard - Dialog intre un filosof, un iudeu si un crestin.pdf
Pierre Abelard - Etica.pdf

./Pierre Aubenque:
Pierre Aubenque - Problema fiintei la Aristotel.pdf

./Pierre Hadot:
Pierre Hadot - Ce este filosofia antica.pdf
Pierre Hadot - Plotin sau simplitatea privirii.pdf

./Pierre Manent:
Pierre Manent - Istoria intelectuala a liberalismului.pdf
Pierre Manent - O filosofie politica pentru cetatean.pdf
Pierre Manent - Originile politicii moderne.pdf

./Pierre Riche:
Pierre Riche - Educatie si cultura in Occidentul barbar.pdf

./Piotr Ceeadaev:
Piotr Ceeadaev - Scrisori filosofice.pdf

./Platon:
Platon - Legile.pdf
Platon - Opere I.pdf
Platon - Opere II.pdf
Platon - Opere III.pdf
Platon - Opere IV.pdf
Platon - Opere V.pdf
Platon - Opere VI.pdf
Platon - Opere VII.pdf
Platon - Scrisorile. Dialoguri suspecte. Dialoguri apocrife.pdf

./Plotin:
Plotin - Enneade, vol. 1.pdf
Plotin - Enneade, vol. 2.pdf
Plotin - Enneade, vol. 3.pdf
Plotin - Opere, vol. 1.pdf
Plotin - Opere, vol. 2.pdf
Plotin - Opere, vol. 3.pdf

./Porfir:
Porfir - Despre naturile inteligibile.pdf

./Porfir Fenicianul:
Porfir Fenicianul - Isagoga.pdf

./Porphyrios:
Porphyrios - Viata lui Pitagora (Viata lui Plotin).pdf

./Proclos:
Proclos - Elemente de teologie.pdf
```

